SHELL :=/bin/bash
cnf ?= .env

include $(cnf)
export $(shell sed 's/=.*//' $(cnf))

GIT_COMMIT := $(shell git rev-parse --short HEAD)

all: build push

build:
	@echo 'Building release $(IMAGE_NAME):$(IMAGE_TAG)'
	docker compose -f docker-compose-build.yml build --build-arg SOURCE_COMMIT=$(GIT_COMMIT)

push:
	az acr login -n lgvudh
	docker push  ${REPOSITORY}/iot/$(IMAGE_NAME):$(IMAGE_TAG)