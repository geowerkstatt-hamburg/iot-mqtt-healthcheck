# Author: Stefan Schuhart
# Description:
# This file does a healthcheck for the mqtt component
# of the UDH FROST-Server and creates a status Observation.
# It creates or reads all needed health check entities
# Version: 1.0.1


# from timeit import default_timer as timer
# import uvloop
# TODO: Error "[code:16] Client or broker did not communicate in the keepalive interval.". Reconnecting in 3 seconds.

import asyncio
import backoff
import dotenv
import json
import logging
import os
import pydantic
import re
import signal
import traceback

# from aiohttp.client_reqrep import ClientRequest
from aiohttp.client import (
    ClientSession, ClientTimeout,
    ClientResponse, ClientConnectionError
)
from aiohttp import ContentTypeError
from asyncio_mqtt import Client, MqttError, MqttCodeError
from contextlib import AsyncExitStack
from datetime import datetime
from pathlib import Path
from typing import Dict, Tuple, Union, Optional
from urllib.parse import urlparse, urlunparse

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s.%(msecs)03d %(levelname)s [%(name)-12s] %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
)

logger = logging.getLogger(__name__)

INVALID_CONTENT = -1
class InvalidURL(Exception):
    "Raised when CientResponse contains no json data."
    ...
    pass

class AppSettings(pydantic.BaseSettings):
    AUTH_HEADER: Optional[dict] = None
    LOG_LEVEL: str = "INFO"
    HTTP_CONNECT_TIMEOUT: float = 3.05  # seconds
    HTTP_READ_TIMEOUT: int = 3  # seconds
    HTTP_TOTAL_TIMEOUT: int = 300  # seconds
    TARGET_FROST_SERVER_HTTP_TEST: str = "http://localhost:8080/FROST-Server/v1.1"
    TARGET_FROST_SERVER_BROKER_ADDRESS: str = "localhost"
    TARGET_FROST_SERVER_BROKER_PORT: int = 1883
    HEALTH_CHECK_INTERVAL: int = 300 # seconds
    HEALTH_SENSOR_NAME: str = "MQTT Health check Sensor"
    HEALTH_OBSPROPERTY_NAME: str = "MQTT Endpoint availability"
    HEALTH_THING_NAME: str = "infrastructure health"
    HEALTH_DATASTREAM_HTTP_NAME: str = "http health check"
    HEALTH_DATASTREAM_MQTT_NAME: str = "mqtt health check"
    KEYCLOAK_FROST_CLIENT_TOKEN_PATH: Optional[str]
    KEYCLOAK_FROST_CLIENT_TOKEN_ACCESS_NAME: Optional[str]
    MQTT_RECONNECT_INTERVAL: Optional[int] = 3
    MQTT_CLIENT_ID_SUFFIX: Optional[str]
    # mqtt_client_id: str = "_".join(filter(None, ("UDH_mqtt_healthcheck", MQTT_CLIENT_ID_SUFFIX)))
    class Config:
        case_sensitive = True
        env_file = '.env'
        env_file_encoding = 'utf-8'

    def print_config(self):
        for setting, value in vars(self).items():
            if not setting.startswith("_"):
                logger.info("Setting %s has value %s", setting, value)


class DervativeSettings(AppSettings):
    mqtt_client_id: str = "UDH_mqtt_healthcheck"
    timeout: ClientTimeout = ClientTimeout()


def handle_results(results, logger: logging.Logger):
    this_logger = logger.getChild("handle_results")
    for result in results:
        if isinstance(result, Exception):
            this_logger.error(f"Handling general error: {repr(result)}")


def handle_exception(loop, context):
    logger = logging.getLogger(f"{__name__}")
    # context["message"] will always be there; but context["exception"] may not
    msg = context.get("exception", context["message"])
    future = context.get("future")
    logger.error("Caught exception: %s at task %s", msg, future)
    logger.debug("Shutting down...")
    asyncio.create_task(shutdown(loop))


async def decode_response_json(
    response: ClientResponse
) -> dict:
    try:
        json_content = await response.json()
    except json.JSONDecodeError:
        logger.error("Error during attempt to read json from response.")
        raise InvalidURL("The response does not contain JSON data. Check the URL: %s", response.url)
    except ContentTypeError:
        logger.error("Error during attempt to read json from response.")
        raise InvalidURL("The response does not contain JSON data. Check the URL: %s", response.url)
    return json_content


async def shutdown(loop, signal: Optional[signal.Signals] = None):
    logger = logging.getLogger(f"{__name__}")
    """Cleanup tasks tied to the service's shutdown."""
    if signal:
        logger.info("Received exit signal %s...", signal.name)
    logger.debug("Closing connections")
    logger.debug("Nacking outstanding messages")
    tasks = [t for t in asyncio.all_tasks() if t is not
             asyncio.current_task()]

    [task.cancel() for task in tasks]

    logger.debug("Cancelling %s outstanding tasks", len(tasks))
    await asyncio.gather(*tasks, return_exceptions=True)
    logger.debug("Flushing metrics")
    loop.stop()


def get_sta_entity_id(response: ClientResponse) -> int:

    entity_id: int = -999
    if entity_location := response.headers.get('location'):
        entity_id = re.findall(r"\(\s*\+?(-?\d+)\s*\)", entity_location)[0]

    return entity_id


def read_secret(secret_path: Path) -> str:

    with secret_path.open("r") as secret_file:
        secret_content: str = secret_file.read()
    logger.debug(f"Successfully loaded {secret_file}.")
    return str(secret_content)


def getEnvVariable(
    variableName: str,
    default: Union[int, str, float] = "",
    sensitive: bool = False
) -> str:

    variableContent: str = str(default)
    try:
        os.environ[variableName]
        if not os.environ[variableName]:
            raise ValueError
        variableContent = os.environ[variableName]
        if not sensitive:
            logger.info(
                f"Variable content retrieved from {variableName}: "
                f"{variableContent}"
            )
        else:
            logger.info("Variable content retrieved from {}".format(variableName))
    except KeyError:
        if not sensitive:
            logger.info(
                f"Environment variable {variableName}"
                f" not found. Set to default {default}")
        else:
            logger.info("Environment variable {} not found. Set to default.")
        # sys.exit()
    except ValueError:
        if not sensitive:
            logger.info(
                f"Environment variable {variableName}"
                f" is empty. Set to default {default}"
            )
        else:
            logger.info(
                f"Environment variable "
                f"{variableName} not found. Set to default."
            )
    return variableContent


def sanitize_url(url: str = "") -> str:
    # split url into components
    parsed_url = urlparse(url)

    # remove unneccessary slashes from path component
    _path = parsed_url.path.strip("/")
    sanitized_url_parts = parsed_url._replace(path=_path)

    # put together again
    complete_url = urlunparse(sanitized_url_parts)

    return complete_url


def read_sta_value(json_data: dict):
    return json_data.get("value", INVALID_CONTENT)


async def create_or_get_entity(
    config: AppSettings,
    entityType: Tuple[str, dict],
    session: ClientSession
) -> int:
        response_data = None
        health_entity_id = None

        if entityType[0] == "Observations":
            url = (
                f"{config.TARGET_FROST_SERVER_HTTP_TEST}/"
                f"Datastreams({entityType[1]['Datastream']['@iot.id']})/Observations"
            )
        else:
            url = (
                f"{config.TARGET_FROST_SERVER_HTTP_TEST}/"
                f"{entityType[0]}?$filter=name eq '{entityType[1]['name']}'"
            )

        response = await session.get(url)
        response_data: dict = await decode_response_json(response)


        # some checks
        values: list = read_sta_value(response_data)
        # error
        if values == INVALID_CONTENT:
            raise InvalidURL("Response does not contain a 'value' key. Check url: %s",
                response.url
            )
        # multiple entries with same name
        if len(values) > 1:
            raise ValueError("Mehr als ein %s existiert mit diesem Namen.", entityType)

        # found an entity
        if len(values) == 1:
            health_entity_id = values[0].get("@iot.id")

        # else create it
        if health_entity_id is None:
            post_response = await session.post(
                f"{config.TARGET_FROST_SERVER_HTTP_TEST}/{entityType[0]}",
                json=entityType[1],
                headers=config.AUTH_HEADER
            )

            health_entity_id = get_sta_entity_id(post_response)

        logger.info(
            f"Health Sensor: {config.TARGET_FROST_SERVER_HTTP_TEST}"
            f"/{entityType[0]}({health_entity_id})")

        return health_entity_id

@backoff.on_exception(
    backoff.expo,
    (
        ClientConnectionError,
        asyncio.TimeoutError,
        ValueError,
        InvalidURL
    ),
    jitter=backoff.full_jitter,
    max_value=3600
)  # never give up
async def create_or_get_healthcheck_entities(config: AppSettings):

    access_token_path_name: Optional[Path] = None
    auth_header: Optional[dict] = None

    if all(ele for ele in (config.KEYCLOAK_FROST_CLIENT_TOKEN_PATH, config.KEYCLOAK_FROST_CLIENT_TOKEN_ACCESS_NAME)):
        access_token_path_name = Path(
            Path(config.KEYCLOAK_FROST_CLIENT_TOKEN_PATH) / Path(config.KEYCLOAK_FROST_CLIENT_TOKEN_ACCESS_NAME)
        )
        access_token: str = read_secret(access_token_path_name)
        config.AUTH_HEADER = {'Authorization': 'Bearer {}'.format(access_token)}

    health_sensor: dict = {
        "description": "Sensor to check the mqtt component availability status for.",
        "encodingType": "http://www.opengis.net/doc/IS/SensorML/2.0",
        "metadata": "This is a virtual sensor only.",
        "name": config.HEALTH_SENSOR_NAME,
    }

    health_observedProperty: dict = {
        "description": (
            "Reports if and when the public mqtt endpoint was available. Transmitted result values are: ['OK']"
        ),
        "definition": "The mqtt endpoint is accessible and users can subscribe to available topics if the availability status is or was recently 'OK'.",
        "name": config.HEALTH_OBSPROPERTY_NAME,
    }

    health_thing: dict[str, Union[str, int, dict, list]] = {
        "name": config.HEALTH_THING_NAME,
        "description": "infrastructure health",
        "properties": {
            "topic": "infrastructure health check",
            "ownerThing": "HH_UDP"
        },
        "Locations": [{
            "name": "health thing location",
            "description": "University of Calgary, CCIT building",
            "encodingType": "application/vnd.geo+json",
            "location": {
                "type": "Point",
                "coordinates": [0, 0]
            }
        }]
    }

    health_datastreams = [("http", {
            "name": config.HEALTH_DATASTREAM_HTTP_NAME,
            "description": (
                "Daten zum Zustand der "
                "HH iot Infrastruktur, HTTP Endpoint"
            ),
            "observationType": (
                "http://www.opengis.net/def/observationType/OGC-OM/2.0/"
                "OM_CategoryObservation"
            ),
            "properties": {
                "metadata": (
                    "https://registry.gdi-de.org/id/de.hh/"
                    "08d1158c-2b5b-49cb-91c3-0ca9fe22a955"
                ),
                "layerName": "http health check",
                "ownerData": "Freie und Hansestadt Hamburg",
                "serviceName": "infrastructure health check",
                "resultNature": "primary",
                "infoLastUpdate": datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
                "mediaMonitored": "Infrastructure availability"
            },
            "unitOfMeasurement": {
                "name": None,
                "symbol": None,
                "definition": None
            }
        }),("mqtt", {
            "name": config.HEALTH_DATASTREAM_MQTT_NAME,
            "description": (
                "Daten zum Zustand der "
                "HH iot Infrastruktur, MQTT Endpoint"
            ),
            "observationType": (
                "http://www.opengis.net/def/observationType/OGC-OM/2.0/"
                "OM_CategoryObservation"
            ),
            "properties": {
                "metadata": (
                    "https://registry.gdi-de.org/id/de.hh"
                    "/08d1158c-2b5b-49cb-91c3-0ca9fe22a955"
                ),
                "layerName": "mqtt health check",
                "ownerData": "Freie und Hansestadt Hamburg",
                "serviceName": "infrastructure health check",
                "resultNature": "primary",
                "infoLastUpdate": datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
                "mediaMonitored": "Infrastructure availability"
            },
            "unitOfMeasurement": {
                "name": "Status",
                "symbol": "",
                "definition": ""
            }
        })]

    health_observation: dict = {
        "phenomenonTime": datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
        "resultTime": datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
        "result": "OK"
    }

    health_entities: dict = {
        "Sensors": health_sensor,
        "ObservedProperties": health_observedProperty,
        "Things": health_thing
    }

    health_entities_id = {}

    async with ClientSession(timeout=config.timeout) as session:

        # check Thing, Sensor, ObsProp
        for entity in health_entities.items():

            health_entities_id[entity[0]] = await create_or_get_entity(
                config, entity, session
            )

        #
        # update DS
        for id in [0,1]:
            health_datastreams[id][1]["Thing"] = {"@iot.id": health_entities_id["Things"]}
            health_datastreams[id][1]["Sensor"] = {"@iot.id": health_entities_id["Sensors"]}
            health_datastreams[id][1]["ObservedProperty"] = {
                "@iot.id": health_entities_id["ObservedProperties"]
            }

        # check DS for http and mqtt exist
        for datastream in health_datastreams:
            try:
                health_entities_id[f"Datastream_{datastream[0]}"] = await create_or_get_entity(
                    config,
                    ("Datastreams", datastream[1]),
                    session
                )
            except ValueError:
                logger.error("An error occured.")
            except InvalidURL:
                logger.error("An error occured.")


        # check initial Observations for http and mqtt exist
        for ObsType in ["http", "mqtt"]:
            health_observation["Datastream"] = {"@iot.id": health_entities_id[f"Datastream_{ObsType}"]}
            health_entities_id[f"Observation_{ObsType}"] = await create_or_get_entity(
                config,
                ("Observations", health_observation),
                session
            )

        return health_entities_id


async def create_http_healthtest_observation(
    url: str,
    session: ClientSession,
    interval: int,
    access_token_path: Optional[Path] = None
):
    while True:
        # authorization
        auth_header: Optional[dict] = None
        if access_token_path:
            access_token: str = read_secret(access_token_path)
            auth_header = {'Authorization': 'Bearer {}'.format(access_token)}

        # test message
        test_observation: dict = {
            "phenomenonTime": datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S.%fZ'),
            "resultTime": datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S.%fZ'),
            "result": "OK"
        }
        logger.debug("Sending http test message.")
        async with session.patch(
            url,
            json=test_observation,
            headers=auth_header
        ) as response:
            date = response.headers.get("DATE")
            log = await response.read()
            if response.status == 200:
                logger.debug(
                    f"HTTP message sent. {date}: {response.url}. Status {response.status}."
                )
            else:
                logger.error(f"Something went wrong: {response.status}, {log}")
            # await response.read()
            await asyncio.sleep(interval)


async def create_mqtt_healthstatus_observation(
    messages,
    template: str,
    session: ClientSession,
    target_frost: str,
    target_Observation_id: str,
    access_token_path: Optional[Path] = None
):
    async for message in messages:
        logger.debug(
            f"Http test message received via MQTT: "
            f"{template.format(message.payload.decode())} "
            f"Topic: {message.topic}. "
            "Creating MQTT health status."
        )
        # authorization
        auth_header: Optional[dict] = None
        if access_token_path:
            access_token: str = read_secret(access_token_path)
            auth_header = {'Authorization': 'Bearer {}'.format(access_token)}

        post_payload = {
            "resultTime": datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.%fZ"),
            "phenomenonTime": json.loads(
                message.payload.decode())['phenomenonTime'],
            "result": "OK"
        }

        logger.debug(f"Sending: {post_payload}")
        async with session.patch(
            url=f"{target_frost}/Observations({target_Observation_id})",
            json=post_payload,
            headers=auth_header
        ) as response:
            log = await response.read()
            if response.status == 200:
                logger.debug("Successfully updated mqtt Status.")
            else:
                logger.error(f"Something went wrong: {response.status}, {log}")


async def log_test_messages(messages, template):
    async for message in messages:
        # 🤔 Note that we assume that the message paylod is an
        # UTF8-encoded string (hence the `bytes.decode` call).
        logger.debug(f"{template.format(message.payload.decode())} {message.topic}")


async def run_periodically(interval, periodic_tasks, parameter):
    while True:
        await asyncio.gather(
            asyncio.sleep(interval), periodic_tasks(parameter)
        )

@backoff.on_exception(
    backoff.expo,
    (
        MqttError,
        MqttCodeError,
        ClientConnectionError,
        asyncio.TimeoutError
    ),
    jitter=backoff.full_jitter,
    max_value=900
)  # never give up
async def health_check(health_entity_ids: dict, config: AppSettings):


    TARGET_DATASTREAM_ID_HTTP_TEST: str = str(
        health_entity_ids["Datastream_http"]
    )

    TARGET_OBSERVATION_ID_HTTP_TEST: str = str(
        health_entity_ids["Observation_http"]
    )

    TARGET_OBSERVATION_ID_MQTT_STATUS: str = str(
        health_entity_ids["Observation_mqtt"]
    )


    access_token_path_name: Optional[Path] = config.AUTH_HEADER

    async with AsyncExitStack() as stack:
        # Keep track of the asyncio tasks that we create, so that
        # we can cancel them on exit
        tasks = set()
        stack.push_async_callback(cancel_tasks, tasks)


        TIMEOUT = config.timeout
        http_client = ClientSession(
            timeout=TIMEOUT
        )

        await stack.enter_async_context(http_client)

        # Connect to the MQTT broker
        mqtt_client = Client(
            hostname=config.TARGET_FROST_SERVER_BROKER_ADDRESS,
            port=config.TARGET_FROST_SERVER_BROKER_PORT,
            client_id="UDH_mqtt_healthcheck_1",
            clean_session=False
        )
        await stack.enter_async_context(mqtt_client)
        logger.info(
            "Connected to broker: tcp://%s:%s",
            mqtt_client._hostname,
            mqtt_client._port
        )
        # Messages that doesn't match a filter will get logged here
        messages = await stack.enter_async_context(
            mqtt_client.messages()
        )

        task = asyncio.create_task(
            create_mqtt_healthstatus_observation(
                messages,
                "[unfiltered] {}",
                http_client,
                config.TARGET_FROST_SERVER_HTTP_TEST,
                TARGET_OBSERVATION_ID_MQTT_STATUS,
                access_token_path_name
            )
        )
        tasks.add(task)

        await mqtt_client.subscribe(
            f"v1.1/Datastreams({TARGET_DATASTREAM_ID_HTTP_TEST})"
            f"/Observations?$select=phenomenonTime"
        )

        # post a test message
        task = asyncio.create_task(
            create_http_healthtest_observation(
                (
                    f"{config.TARGET_FROST_SERVER_HTTP_TEST}"
                    f"/Observations({TARGET_OBSERVATION_ID_HTTP_TEST})"
                ),
                http_client,
                config.HEALTH_CHECK_INTERVAL,
                access_token_path_name
            )
        )
        tasks.add(task)

        await asyncio.gather(*tasks, return_exceptions=False)


async def cancel_tasks(tasks):
    for task in tasks:
        if task.done():
            continue
        task.cancel()
        try:
            await task
        except asyncio.CancelledError:
            pass


if __name__ == "__main__":
    # uvloop.install()
    dotenv.load_dotenv()
    settings = AppSettings()
    more_settings = DervativeSettings()
    more_settings.mqtt_client_id = "_".join(
        filter(
            None, (more_settings.mqtt_client_id, settings.MQTT_CLIENT_ID_SUFFIX)
        )
    )
    more_settings.timeout = timeout = ClientTimeout(
        sock_connect=settings.HTTP_CONNECT_TIMEOUT,
        sock_read=settings.HTTP_READ_TIMEOUT,
        total=settings.HTTP_TOTAL_TIMEOUT
    )

    more_settings.print_config()

    LOG_LEVEL = getEnvVariable("LOG_LEVEL", "INFO")
    logger.setLevel(LOG_LEVEL)

    logging.getLogger('backoff').addHandler(logging.StreamHandler())
    logging.getLogger('backoff').setLevel(LOG_LEVEL)

    loop = asyncio.get_event_loop()
    if more_settings.LOG_LEVEL == "DEBUG":
        loop.set_debug(True)

    signals = (signal.SIGHUP, signal.SIGTERM, signal.SIGINT)
    for s in signals:
        loop.add_signal_handler(
            s, lambda s=s: asyncio.create_task(shutdown(loop, signal=s)))

    loop.set_exception_handler(handle_exception)

    try:
        logger.info("Beginning to check for health entities.")
        health_entity_ids = loop.run_until_complete(create_or_get_healthcheck_entities(more_settings))

        logger.info("Done with checking for health entities. "
            "Beginning normal operation with sending health Observations."
        )
        main_loop = loop.create_task(health_check(health_entity_ids, more_settings))
        loop.run_forever()
    finally:
        loop.close()
        logger.info("Successfully shutdown.")
