# docker
## build

```bash
docker compose -f dc_mqtt_health_build.yml build \
--build-arg SOURCE_COMMIT=$(git rev-parse --short HEAD)
```

## create an Azure SP for pulling containers of from an Azure registry

... and give it AcrPull role

```bash
#!/bin/bash
# This script requires Azure CLI version 2.25.0 or later. Check version with `az --version`.

# Modify for your environment.
# ACR_NAME: The name of your Azure Container Registry
# SERVICE_PRINCIPAL_NAME: Must be unique within your AD tenant
ACR_NAME=$1
SERVICE_PRINCIPAL_NAME=$2

echo "Azure cr: $ACR_NAME"
# Obtain the full registry ID
ACR_REGISTRY_ID=$(az acr show --name $ACR_NAME --query "id" --output tsv --subscription "HH LGV MCI")
# echo $registryId

# Create the service principal with rights scoped to the registry.
# Default permissions are for docker pull access. Modify the '--role'
# argument value as desired:
# acrpull:     pull only
# acrpush:     push and pull
# owner:       push, pull, and assign roles
PASSWORD=$(az ad sp create-for-rbac --name "$SERVICE_PRINCIPAL_NAME" --scopes $ACR_REGISTRY_ID --role acrpull --query "password" --output tsv)
USER_NAME=$(az ad sp list --display-name $SERVICE_PRINCIPAL_NAME --query "[].appId" --output tsv)

# Output the service principal's credentials; use these in your services and
# applications to authenticate to the container registry.
echo "Service principal ID: $USER_NAME"
echo "Service principal password: $PASSWORD"
```

## create a k8s pull secret

 Before you begin, you need to get corresponding service principal (client) ID and password.
 
```bash
#!/bin/bash
kubectl create secret docker-registry azurecr-lgvudh \
    --namespace healthchecks \
    --docker-server=lgvudh.azurecr.io \
    --docker-username=d18b9d90-8a79-49f0-9b3d-ff7bd1f939bd \
    --docker-password=''

```