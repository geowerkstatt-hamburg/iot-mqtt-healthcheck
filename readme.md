# HH_UDP_IoT mqtt health check
## Description
This repo contains a python script which sends a http test message to a specific http-Test Datastream, listens to that specific endpoint via mqtt and on successfully recieving a message, sends a status update to another Datastream (mqtt health). The mqtt status message will only be send, if the test message was recieved.

All neccessary entities will be created if not already there. This script will check for existence of these entities by name. The log will contain a list of all created/received endpoints (Thing, ObservedProperty, Sensor etc.)

Currently all Observations (mqtt status, http test) will be created once and will be updated via HTTP PUT.

The health check works fully async by using the new async await syntax leveraged by the python asyncio library.
## Configuration

This script runs inside a docker container. Configuration is possible via environemnt variables which have to be set via docker-compose.

|name|description|default|
|---|---|---|
|TARGET_FROST_SERVER_HTTP_TEST| hostname and Version path for the http test message and for creating/reveiving all health entities|"http://frost-http:8080/FROST-Server/v1.1" | full path with Version
|TARGET_FROST_SERVER_BROKER_HOST | hostname  the mqtt broker listens on| "frost-mqtt"
|TARGET_FROST_SERVER_BROKER_PORT | port the mqtt broker listens on| 1885
|HEALTH_CHECK_INTERVAL| interval in seconds a test message will be send|10
|MQTT_RECONNECT_INTERVAL | reconnect to the broker after waiting for MQTT_RECONNECT_INTERVAL in seconds, if connection is lost | 3
|HEALTH_SENSOR_NAME | name of the health sensor | MQTT Health check Sensor
|HEALTH_OBSPROPERTY_NAME | name of the Observed Property| Zustand
|HEALTH_THING_NAME | name of the thing| infrastructure health
|HEALTH_DATASTREAM_HTTP_NAME | name of the Datastream for the http test message| http health check
|HEALTH_DATASTREAM_MQTT_NAME | name of the Datastream for the mqtt status message| mqtt health check
|KEYCLOAK_FROST_CLIENT_TOKEN_PATH: | access token path inside container| not set, will be ignored if empty
|KEYCLOAK_FROST_CLIENT_TOKEN_ACCESS_NAME| acces token name inside container| not set, will be ignored if empty
|LOG_LEVEL | customizes the log level | INFO

## Automate the the mqtt status check
To check for the mqtt status, check the log to get the mqtt status Datastream id. Then make a HTTP GET request i.e. to:

https://{HOST\[/FROST-Server\]}/v1.1/Datastreams({MQTT_STATUS_ID})?$select=result&$filter=resultTime gt now() sub duration'PT5M'

This path will show you all Observations which where updated in the last 5 Minutes (by checking resultTime)and selects only the 'result' entry of the Observation. Response status code should be equal to 200 and json response should contain ```{"value": [{"result": "OK"}]}``` if there is no Observation within the specified Interval (in the last 5 Minutes) response will containe no values (```{"value": []}```).

This GET-request Interval and HEALTH_CHECK_INTERVAL should correspond. E.g. set HEALTH_CHECK_INTERVAL to 60 seconds and check for the status within a 5 minute interval.

## using the helm chart
### Naming
Release: Name should contain the app name + environment, i.e. mqtt-healthcheck-qs or mqtt-healthcheck-tld-prod

### installing

Install from local source (for testing only), with

```bash
helm upgrade --install healthcheck-test /path/to/hh-sta-mqtt-healthcheck -f values.yaml
```

Install from helm repository:

```bash
helm upgrade --install \
healthcheck-test \
udp-mqtt-healthcheck/hh-sta-mqtt-healthcheck \
-f /path/to/local/values.yaml
```

For adding this helm chart repository to your local repo list:

```bash
helm repo add udp-mqtt-healthcheck \
https://api.bitbucket.org/2.0/repositories/geowerkstatt-hamburg/iot-mqtt-healthcheck/src/Prod \
--username YOUR_BITBUCKET_USERNAME
```