Application {{ .Chart.Name }} started.
Release: {{ .Release.Name}}
Namespace: {{ .Release.Namespace }}
Revision: {{ .Release.Revision }}

Try:
    $ helm status {{ .Release.Name }}
    $ helm get all {{ .Release.Name }}