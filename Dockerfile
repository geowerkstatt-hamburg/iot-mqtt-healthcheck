FROM lgvudh.azurecr.io/iot/python-connector-base:3.9.14

ARG SOURCE_COMMIT
LABEL vendor="UDH" \
      maintainer="UDH" \
      name="iot/mqtt_healtcheck" \
      appVersion="1.4.2"
LABEL SOURCE_COMMIT=$SOURCE_COMMIT

ARG USERNAME=pythonuser
USER $USERNAME

WORKDIR /home/pythonuser

COPY requirements.txt ./requirements.txt

USER root
RUN apt-get update && apt-get upgrade -y \
    && /usr/local/bin/python -m pip install --upgrade pip \
    && /usr/local/bin/pip install \
        --requirement "requirements.txt" \
        --no-cache-dir \
    && rm -r \
        "requirements.txt"
USER $USERNAME
COPY mqtt-healthcheck.py ./mqtt-healthcheck.py

ENTRYPOINT ["python","-u"]
CMD [ "mqtt-healthcheck.py" ]
